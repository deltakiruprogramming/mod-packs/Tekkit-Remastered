val glowstone = <minecraft:glowstone_dust>;
val fish = <ore:listAllfishraw>; // Will research this later
val lantern = <minecraft:sea_lantern>;

recipes.removeShaped(lantern);
print("Removed the old Sea Lantern recipe.");
recipes.addShapeless(lantern * 4, [fish, glowstone]);
print("Added the new Sea Lantern recipe.");
print("Done executing!");
