val rotten_flesh = <minecraft:rotten_flesh>;
val mob_chunk = <mysticalagriculture:tier1_mob_chunk>;

recipes.addShapeless(mob_chunk * 4, [rotten_flesh]);
