val iron = <ore:ingotIron>;
val lantern = <minecraft:sea_lantern>;
val obsidian = <minecraft:obsidian>;
val obsidian2 = <tp:reinforced_obsidian>;
val crystal1 = <tp:growth_block>;
val crystal2 = <tp:growth_upgrade>;
val crystal3 = <tp:growth_upgrade_two>;

recipes.removeShaped(crystal1);
recipes.removeShaped(crystal2);
recipes.removeShapeless(crystal3);

recipes.addShaped(crystal1, [[iron, iron, iron], [iron, lantern, iron], [iron, iron, iron]]);
recipes.addShaped(crystal2, [[obsidian, obsidian, obsidian], [obsidian, crystal1, obsidian], [obsidian, obsidian, obsidian]]);
recipes.addShaped(crystal3, [[obsidian2, obsidian2, obsidian2], [obsidian2, crystal2, obsidian2], [obsidian2, obsidian2, obsidian2]]);
