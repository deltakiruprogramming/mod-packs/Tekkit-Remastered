val purpleSlime = <tconstruct:edible:2>;
val slime = <ore:slimeball>;
val cslime = <ore:slimeballnopurple>;
val purpleDye = <minecraft:dye:5>;

cslime.addAll(slime);
cslime.remove(purpleSlime);

recipes.addShapeless(purpleSlime, [cslime, purpleDye]);
