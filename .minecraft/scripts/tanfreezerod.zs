val freezeRod = <toughasnails:freeze_rod>;
val iceCube = <toughasnails:ice_cube>;
val blazePowder = <minecraft:blaze_powder>;

recipes.addShapeless(freezeRod * 2, [blazePowder, iceCube]);
